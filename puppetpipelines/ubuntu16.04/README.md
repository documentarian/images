# PowerShell Image for Puppet Pipelines

This Dockerfile builds an image suitable for use in Puppet Pipelines and includes `pwsh`, `calibre`, and `npm`.